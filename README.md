# Wardrobify

Team:

* Person 1 - Jeongjae shoes
* Person 2 - Ben hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Hats Microservice
The Hats microservice is a crucial component of the Wardrobify system, responsible for managing and providing information about various hats in a user's wardrobe. This README provides an in-depth understanding of the Hats microservice, its components, and how it interacts with the rest of the system.

Hat Model Entity
In the Hats microservice, we have designed a Hat model entity that allows us to uniquely identify and manage hats within the microservice's specific database. The Hat model includes several key attributes:

fabric: Describes the material from which the hat is made.
style_name: Represents the style or type of the hat.
color: Indicates the color or color combination of the hat.
picture_url: Stores the URL of an image associated with the hat.
id: Serves as a unique identifier for each hat.
Additionally, we have established a relationship between hats and their respective locations. This relationship is represented by a foreign key:

location: A foreign key linking each hat to its associated location.
Location Value Object (LocationVO)
To ensure data integrity and immutability, we introduced the concept of a Location Value Object (LocationVO). The LocationVO is used to represent location-specific data within the microservice. It is designed to be immutable and fungible, meaning it does not actively participate in data evolution but serves as reference data.

Key attributes within the LocationVO include:

import_href: A unique identifier for each location, which can be used for referencing.
closet_name: The name or identifier of the closet where the hat is stored.
section_number: Indicates the section or area within the closet.
shelf_number: Specifies the shelf where the hat is placed.
Integration with Wardrobe Microservice
Seamless integration with the Wardrobe microservice is a core feature of the Hats microservice. To achieve this integration, we have implemented a poller mechanism. The poller periodically requests data from the Wardrobe API endpoint. The primary objectives of this integration are as follows:

Data Synchronization: By periodically polling the Wardrobe API, we ensure that our own LocationVO database remains up-to-date without altering the core Location data in the Wardrobe API.

Reference Data Management: The Hats microservice relies on the Wardrobe API for location-related information, and the LocationVO database acts as a reference for this data.

Seamless User Experience: This integration enables users to access and manage hats within their wardrobe while ensuring that location information is consistent and accurate.

Conclusion
The Hats microservice is a fundamental part of the Wardrobify ecosystem, offering a robust and efficient solution for managing hats within a user's wardrobe. Its integration with the Wardrobe microservice ensures data consistency and enhances the overall user experience.

For further details on API endpoints, usage instructions, or technical documentation, please refer to the relevant documentation and API guides.
