from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
   import_href = models.CharField(max_length=200, unique=True, null=True)
   name = models.CharField(max_length=200, null= True)

class Hats(models.Model):
   hat_name = models.CharField(max_length=200, null=True)
   fabric = models.CharField(max_length=200, null=True)
   style = models.CharField(max_length=200, null=True)
   color = models.CharField(max_length=200, null=True)
   picture_url = models.URLField(max_length=500, null=True)
   location = models.ForeignKey(
      LocationVO,
      related_name="hats",
      on_delete=models.CASCADE,
      null=True,
   )
   def get_api_url(self):
        return reverse("api_show_attendee", kwargs={"pk": self.pk})
