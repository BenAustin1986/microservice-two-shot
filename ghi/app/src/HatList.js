import React, { useEffect, useState } from 'react';
function HatList() {
    const [hats, setHats] = useState([]);
    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/');
        if (response.ok) {
            const { hats } = await response.json();
            setHats(hats);
        } else {
            console.error('An error occurred fetching the data');
        }
    };
    async function handleDelete(id) {
        try {
          const response = await fetch(`http://localhost:8090/api/hats/${id}/`, {
            method: "DELETE"
          });
          if (response.ok) {
            getData(); 
          } else {
          }
        } catch (error) {
          console.error('An unexpected error occurred:', error);
        }
      }
    useEffect(() => {
        getData();
    }, []);
    return (
        <div className="my-5 container">
            <div className="row">
                <h1>Hat List</h1>
                <table className="table table-striped m-3">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Hat style</th>
                            <th>Color</th>
                            <th>Fabric</th>
                            <th>Picture</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {hats.map(hats => (
                            <tr key={hats.id}>
                                <td>{hats.location}</td>
                                <td>{hats.style}</td>
                                <td>{hats.color}</td>
                                <td>{hats.fabric}</td>
                                <td>
                                    <img
                                        src={hats.picture_url}
                                        alt=""
                                        width="100px"
                                        height="100px"
                                    />
                                </td>
                                <td>
                                    <button
                                        onClick={() => handleDelete(hats.id)}
                                        className="btn btn-danger"
                                    >Delete</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
export default HatList;
