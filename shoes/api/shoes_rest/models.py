from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)

class Shoes(models.Model):
    fabric = models.CharField(max_length=200, null=True)
    style_name = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=100, null=True)
    picture_url = models.URLField(null=True)
    shoe_name = models.CharField(max_length=200, null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name='shoes',
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.style_name

    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"pk":self.pk})

# Create your models here.
