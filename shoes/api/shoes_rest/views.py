from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO, Shoes

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href"
    ]

class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "fabric",
        "style_name",
        "color",
        "bin",
        "id",
        "picture_url",
        "shoe_name",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesDetailEncoder
        )

    else:
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(id=content['bin'])
            content["bin"]=bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400
            )
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            {"shoe": shoes},
            encoder=ShoesDetailEncoder,
            safe=False,
)

@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=pk)
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoesDetailEncoder,
        )
    elif request.method == "DELETE":
        try:
            shoe = Shoes.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoesDetailEncoder,
                safe=False
            )

        except Shoes.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

# Create your views here.
