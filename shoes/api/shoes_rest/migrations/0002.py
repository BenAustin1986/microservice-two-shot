from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0001'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shoes',
            name='color',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='shoes',
            name='fabric',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='shoes',
            name='bin',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='shoes', to='shoes_rest.binvo'),
        ),
        migrations.AlterField(
            model_name='shoes',
            name='picture_url',
            field=models.URLField(null=True),
        ),
        migrations.AlterField(
            model_name='shoes',
            name='style_name',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='binvo',
            name='closet_name',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='binvo',
            name='import_href',
            field=models.CharField(max_length=200, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='binvo',
            name='bin_number',
            field=models.PositiveSmallIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='binvo',
            name='bin_size',
            field=models.PositiveSmallIntegerField(null=True),
        ),
    ]
