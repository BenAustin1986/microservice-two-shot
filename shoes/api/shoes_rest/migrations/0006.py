from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0005'),
    ]

    operations = [
        migrations.AddField(
            model_name='shoes',
            name='shoe_name',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
